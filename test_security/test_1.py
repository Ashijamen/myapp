import pylint.lint

def run_pylint(filename):
    """
    Uruchamia Pylint na określonym pliku i wyświetla wszystkie błędy lub ostrzeżenia.

    Args:
        filename (str): Nazwa pliku do analizy.

    Returns:
        bool: True, jeśli nie znaleziono żadnych błędów ani ostrzeżeń, False w przeciwnym razie.
    """
    try:
        pylint_output = pylint.lint.Run([filename])
        
        if pylint_output.linter.stats['error'] or pylint_output.linter.stats['warning']:
            for message in pylint_output.linter.reporter.msgs:
                print(message)
            return False
        
        print("Pylint zakończył działanie pomyślnie bez błędów ani ostrzeżeń.")
        return True
    
    except Exception as e:
        print(f"Wystąpił błąd: {e}")
        return False

if __name__ == "__main__":
    filename = "app.py"
    if run_pylint(filename):
        print("Pylint przebiegł pomyślnie. Twój kod spełnia standardy.")
    else:
        print("Pylint znalazł błędy lub ostrzeżenia w Twoim kodzie.")


