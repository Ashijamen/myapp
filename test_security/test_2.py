import subprocess
import sys
import os

def install_safety():
    print("Instalacja safety...")
    subprocess.run([sys.executable, "-m", "pip", "install", "safety"])

def dependency_scanning_with_safety():
    print("\n")
    print("\nSCA - Skanowanie zalezności - Safety")
    print("\n")
    python_executable = sys.executable
    requirements_path = os.path.join(os.path.dirname(__file__), 'requirements.txt')  
    try:
        subprocess.run([python_executable, "-m", "safety", "check", "-r", requirements_path])  
    except subprocess.CalledProcessError as e:
        print(f"Błąd podczas uruchamiania Safety: {e}")
        sys.exit(1)

if __name__ == "__main__":
    try:
        import safety
    except ImportError:
        install_safety()
    dependency_scanning_with_safety()