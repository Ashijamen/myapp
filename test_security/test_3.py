import requests

# Adres lokalnego serwera Flask
BASE_URL = 'http://127.0.0.1:5000'

# Funkcja do wysyłania żądań GET i POST
def send_request(method, endpoint, data=None):
    url = BASE_URL + endpoint
    if method == 'GET':
        response = requests.get(url)
    elif method == 'POST':
        response = requests.post(url, data=data)
    return response

# Testy DAST
def run_dast():
    print("Starting DAST tests...\n")

    # Testy GET
    print("Testing GET requests...\n")

    # Test strony głównej
    response = send_request('GET', '/')
    print("GET / - Status code:", response.status_code)

    # Test strony dodawania kontaktu
    response = send_request('GET', '/add_contact')
    print("GET /add_contact - Status code:", response.status_code)

    # Testy POST
    print("\nTesting POST requests...\n")

    # Test dodawania kontaktu z poprawnymi danymi
    data = {'name': 'Test User', 'phone': '123456789'}
    response = send_request('POST', '/add_contact', data=data)
    print("POST /add_contact (valid data) - Status code:", response.status_code)

    # Test dodawania kontaktu z nieprawidłowymi danymi
    data = {'name': 'Test User', 'phone': '12345'}  # Nieprawidłowy numer telefonu
    response = send_request('POST', '/add_contact', data=data)
    print("POST /add_contact (invalid data) - Status code:", response.status_code)

    # Test edycji kontaktu
    # Zakładamy, że wcześniej dodano kontakt o nazwie "Test User"
    data = {'name': 'Test User', 'phone': '987654321'}  # Nowy numer telefonu
    response = send_request('POST', '/edit_contact/Test%20User', data=data)
    print("POST /edit_contact/Test%20User - Status code:", response.status_code)

    # Test usuwania kontaktu
    # Zakładamy, że wcześniej dodano kontakt o nazwie "Test User"
    response = send_request('GET', '/delete_contact/Test%20User')
    print("GET /delete_contact/Test%20User - Status code:", response.status_code)

    print("\nDAST tests completed.")

if __name__ == '__main__':
    run_dast()
